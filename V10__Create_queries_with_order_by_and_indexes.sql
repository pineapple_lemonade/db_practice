/*Создание индекса для поиска людей заданной профессии*/
CREATE INDEX idx_role_name ON workers USING hash(role_name);

/*Создание индекса для поиска количества заказов*/
CREATE INDEX idx_amount_of_orders ON workers_orders USING btree(worker_id);

/*Наибольшее количество работников на заказе и его айди*/
SELECT max(amount) amount_of_workers, order_id FROM (SELECT order_id, count(order_id) amount FROM workers_orders GROUP BY order_id) as oic
GROUP BY order_id LIMIT 1;

/*Отсортировать по количеству работников в департаменте*/
SELECT max(amount), department_id FROM (SELECT department_id, count(department_id) amount FROM workers GROUP BY department_id) as dia
GROUP BY department_id;