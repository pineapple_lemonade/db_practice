WITH RECURSIVE rec_find_relations(worker_id) AS(
       SELECT worker_id, order_id FROM workers_orders WHERE worker_id = 1
    UNION
    SELECT workers_orders.worker_id, workers_orders.order_id FROM workers_orders
    JOIN rec_find_relations ON workers_orders.worker_id = rec_find_relations.worker_id
) SELECT * FROM rec_find_relations;

WITH RECURSIVE w(worker_id) AS(
    SELECT worker_id FROM workers_orders
    UNION
    SELECT order_id FROM workers_orders JOIN w ON w.worker_id = workers_orders.order_id
) SELECT * FROM w;
