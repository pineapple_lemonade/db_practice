/*Найти связь от кого получил заказ работник*/
SELECT workers_orders.worker_id, customers.customer_id FROM workers_orders
JOIN customers ON workers_orders.order_id = customers.order_id;

/*Вывести список всех работников и их занятость(если не заняты то order_id = null)*/
SELECT workers.worker_id, workers_orders.order_id FROM workers
LEFT JOIN workers_orders on workers.worker_id = workers_orders.worker_id;

/*Вывести список занятости всех специализаций работников*/
SELECT workers.worker_id, workers_orders.order_id, workers.role_name FROM workers_orders
RIGHT JOIN workers on workers.worker_id = workers_orders.worker_id;

/*Вывести полную статистику о текущем состоянии рекламной компании*/
SELECT * FROM workers_orders
FULL JOIN workers ON workers_orders.worker_id = workers.worker_id;

/*Вывести список со всевозможным распределением людей на проекты*/
SELECT * FROM workers CROSS JOIN orders;

/*Вывести по одному работнику каждой специализации для определения на крупный проект(founder не считается специализацией)*/
SELECT * FROM roles NATURAL JOIN workers;

/*self join*/
SELECT w1.worker_id, w1.department_id FROM workers w1
JOIN workers w2 on w2.worker_id = w1.department_id;

/*semi join вывести всех работников с заказами*/
SELECT w.worker_id FROM workers w
WHERE EXISTS (SELECT 1 FROM workers_orders wo WHERE w.worker_id = wo.worker_id);

/*anti join без заказов*/
SELECT w.worker_id FROM workers w
WHERE NOT EXISTS (SELECT 1 FROM workers_orders wo WHERE w.worker_id = wo.worker_id);