/*создать вью с текущей занятостью и состоянем компании*/
CREATE MATERIALIZED VIEW show_statistic(worker_id) AS
SELECT workers.worker_id, order_id, role_name FROM workers_orders FULL JOIN workers ON workers_orders.worker_id = workers.worker_id;
SELECT * FROM show_statistic;
/*создать вью с отображением какому подразделению какие заказы принадлежат*/
CREATE MATERIALIZED VIEW show_departments_orders AS
SELECT order_id, w.department_id FROM workers_orders
JOIN workers w on w.worker_id = workers_orders.worker_id;