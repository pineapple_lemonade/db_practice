WITH cte_workers_orders AS(
    SELECT worker_id,(
        CASE
            WHEN worker_id in
(select worker_id from workers_orders group by worker_id having count(*) > 1) THEN 'hard worker'
            ELSE 'weak'
               END) status
    FROM workers_orders
) SELECT DISTINCT * FROM cte_workers_orders WHERE status = 'hard worker';

WITH cte_workers_departments AS(
    SELECT worker_id,(
        CASE
            WHEN department_id = 1 THEN 'MOVE'
            WHEN department_id = 2 THEN 'VIVAT'
            ELSE 'no name'
              END) department_name
    FROM workers
) SELECT * FROM cte_workers_departments;

