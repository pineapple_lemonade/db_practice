create view find_all_orders_where_workers_more_than_two
as select * from workers_orders where order_id in
(select order_id from workers_orders group by order_id having count(*) > 1)
with local check option;