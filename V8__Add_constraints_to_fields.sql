/*очевидно должно быть уникальное, иначе нет смысла создавать 2 одинаковых*/
ALTER TABLE departments ADD CONSTRAINT unique_constraint UNIQUE(spec_name);
/*чтобы исключать сокращения*/
ALTER TABLE departments ADD CONSTRAINT length_check_constraint CHECK ( char_length(spec_name) > 2 );
/*чтобы исключать сокращения*/
ALTER TABLE roles ADD CONSTRAINT length_check_constraint CHECK ( char_length(role_name) > 2 );


