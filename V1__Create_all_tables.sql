create table roles(
	role_id serial primary key,
	role_name varchar(100) unique not null
);

create table departments(
	department_id serial primary key,
	spec_name varchar(100) unique not null
);

create table workers(
	worker_id serial primary key,
	department_id integer,
	role_name varchar(100),
	foreign key (department_id) references departments(department_id),
	foreign key (role_name) references roles(role_name)
);

create table orders(
	order_id serial primary key
);

create table customers(
	customer_id serial primary key,
	order_id integer,
	foreign key (order_id) references orders(order_id)
);

create table workers_orders(
	worker_id integer,
	order_id integer,
	foreign key (worker_id) references workers(worker_id),
	foreign key (order_id) references orders(order_id)
);