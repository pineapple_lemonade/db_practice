insert into roles (role_name) values ('manager');

insert into roles (role_name) values ('admin');

insert into roles (role_name) values ('founder');

insert into roles (role_name) values ('designer');

insert into departments(spec_name) values ('web ad');

insert into departments(spec_name) values ('billboards');

insert into workers (department_id, role_name) values ((select department_id from departments where department_id = 1), (select role_name from roles where role_name = 'designer'));

insert into workers (department_id, role_name) values ((select department_id from departments where department_id = 1), (select role_name from roles where role_name = 'manager'));

insert into workers (department_id, role_name) values ((select department_id from departments where department_id = 2), (select role_name from roles where role_name = 'admin'));

insert into workers (department_id, role_name) values ((select department_id from departments where department_id = 2), (select role_name from roles where role_name = 'manager'));

insert into orders (order_id) values (1);

insert into orders (order_id) values (2);

insert into orders (order_id) values (3);

insert into customers (order_id) values ((select order_id from orders where order_id = 1));

insert into customers (order_id) values ((select order_id from orders where order_id = 2));	

insert into customers (order_id) values ((select order_id from orders where order_id = 3));	

insert into workers_orders (worker_id, order_id) values ((select worker_id from workers where worker_id = 1), (select order_id from orders where order_id = 1));

insert into workers_orders (worker_id, order_id) values ((select worker_id from workers where worker_id = 1), (select order_id from orders where order_id = 2));	

insert into workers_orders (worker_id, order_id) values ((select worker_id from workers where worker_id = 2), (select order_id from orders where order_id = 3));

insert into workers_orders (worker_id, order_id) values ((select worker_id from workers where worker_id = 3), (select order_id from orders where order_id = 3));	
